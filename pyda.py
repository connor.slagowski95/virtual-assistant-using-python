"""Version 3.0"""

import wikipedia
import wolframalpha
import pyttsx3

def program(question):

    engine = pyttsx3.init()
    try:
        app_id = "4E63EW-UE8G6PETAV"
        client = wolframalpha.Client(app_id)

        result = client.query(question)
        answer = next(result.results).text

        print(answer)
        engine.say(answer)
        engine.runAndWait()
    except:
        question = question.split(' ')
        question = " ".join(question[2:])
        answer = print(wikipedia.summary(question, sentences=2))
        engine.say("I searched the internet and found this answer.")
        engine.runAndWait()