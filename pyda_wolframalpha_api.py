import wolframalpha

question = input("Question:  ")
app_id = "4E63EW-UE8G6PETAV"
client = wolframalpha.Client(app_id)

result = client.query(question)
answer = next(result.results).text

print(answer)
